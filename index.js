// Activity s23

// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/



let trainer = {
		name: 'Ash Kechum',
		age: 19,
		friends : {
			Hoenn: ['Max' , 'May'],
			Kanto: ['Brock' , 'Misty']
		},

			

			
		pokemon:['Pikachu', 'Charizard' , 'Squirtle' , 'Balbasaur'] ,
		
		talk: function() {
			console.log(this.pokemon + " I Choose you");
		}
	}
	console.log(trainer);
	console.log("Result of dot notation: ");
	console.log(trainer.name);

	console.log("Result of bracket notation: ");
	console.log(trainer['pokemon']);


//////////////////////////////////////
	function choose(pokeType){
	let monster = trainer['pokemon'].includes(pokeType);

		
	if (monster === true){
		
		console.log( pokeType + " I Choose you!");

	}else {
		
		alert ("You don't have that pokemon");
	}
}
///////////////////////////////////////






function Pokemon(name, level ) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
    	let damage = target.health - this.attack;

        console.log(this.name + ' tackled ' + target.name);
        console.log("targetPokemon's health is now " + damage);
       
        if (damage <= 0 ) {
        	console.log(target.faint());
        }
        
        }
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }

}


let pikachu = new Pokemon ('Pikachu' , 50 );
let squirtle = new Pokemon ('Squirtle' , 20 );
let charmeleon = new Pokemon ('Charmeleon' , 45);
let butterfree = new Pokemon ('Butterfree' , 37);

// pikachu.tackle	(squirtle);/



// function fight


// function fight

// console.log(firstPokemon.tackle(firstPokemon.name) + secondPokemon.name );
/*
function Pokemon(name, level , health , attack , tackle , faint) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}






// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon('Rattata', 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);*/